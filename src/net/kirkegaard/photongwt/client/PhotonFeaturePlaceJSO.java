package net.kirkegaard.photongwt.client;

import com.google.gwt.core.client.JavaScriptObject;

public class PhotonFeaturePlaceJSO extends JavaScriptObject {
	
	protected PhotonFeaturePlaceJSO() { }
	
	public final native String getStreet() /*-{
	  return this.properties.street;
	}-*/;
	
	public final native String getHousenumber() /*-{
	  return this.properties.housenumber;
	}-*/;
	
	public final native String getPostcode() /*-{
	  return this.properties.postcode;
	}-*/;
	
	public final native String getCity() /*-{
	  return this.properties.city;
	}-*/;
	
	public final native boolean isHouse() /*-{
	  return this.properties.osm_value === "house";
	}-*/;

}
