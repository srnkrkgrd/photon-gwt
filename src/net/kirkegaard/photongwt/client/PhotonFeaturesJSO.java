package net.kirkegaard.photongwt.client;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsonUtils;

public class PhotonFeaturesJSO extends JavaScriptObject {
	
	protected PhotonFeaturesJSO() { }
	
	public static PhotonFeaturesJSO fromJson(String json) {
		return (PhotonFeaturesJSO) JsonUtils.safeEval(json);
	}

	public final native int length() /*-{
	  return this.features.length;
	}-*/;
	
	public final native PhotonFeatureJSO get(int i) /*-{
	  return this.features[i];
	}-*/;
}

