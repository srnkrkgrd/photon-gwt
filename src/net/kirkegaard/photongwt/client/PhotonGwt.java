package net.kirkegaard.photongwt.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public class PhotonGwt implements EntryPoint {

	private static final String ENDPOINT = "http://photon.komoot.de/api/?q=";
	private static final String QUERY = URL.encode("Absalonsvej");
	private static final String LIMIT = "&limit=100";
	
	Label label = new Label();
	
	public void onModuleLoad() {
		RootPanel.get("main").add(label);
				
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, ENDPOINT + QUERY + LIMIT);
		builder.setCallback(requestCallback);
		try {
			Request request = builder.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}
	
	}
	
	private RequestCallback requestCallback = new RequestCallback() {
		
		@Override
		public void onResponseReceived(Request request, Response response) {
			String json = response.getText();
			String text = "";
			
			PhotonFeaturesJSO features = PhotonFeaturesJSO.fromJson(json);
			for(int i=0; i< features.length(); i++) {
				PhotonFeatureJSO f = features.get(i);
				if(f.isPlace()) {
					PhotonFeaturePlaceJSO place = f.asPlace();
					if(place.isHouse()) {
						text = text + place.getStreet() + " " + place.getHousenumber() + " " + place.getPostcode() + " " + place.getCity() + ", ";
					}
				} else if (f.isHighway()) {
					PhotonFeatureHighwayJSO highway = f.asHighway();
					text = text + highway.getName() + " " + highway.getPostcode() +  " " + highway.getCity() + ", ";
				}
			}
			label.setText(text);		}
		
		@Override
		public void onError(Request request, Throwable exception) {
			
		}
	};

}
