package net.kirkegaard.photongwt.client;

import com.google.gwt.core.client.JavaScriptObject;

public class PhotonFeatureHighwayJSO extends JavaScriptObject {
	
	protected PhotonFeatureHighwayJSO() { }
	
	public final native String getName() /*-{
	  return this.properties.name;
	}-*/;
		
	public final native String getPostcode() /*-{
	  return this.properties.postcode;
	}-*/;
	
	public final native String getCity() /*-{
	  return this.properties.city;
	}-*/;
	
	public final native boolean isResidential() /*-{
	  return this.properties.osm_value === "residential";
	}-*/;

}
