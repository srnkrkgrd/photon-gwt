package net.kirkegaard.photongwt.client;

import com.google.gwt.core.client.JavaScriptObject;

public class PhotonFeatureJSO extends JavaScriptObject {
	
	protected PhotonFeatureJSO() { }
	
	public final native PhotonFeaturePlaceJSO asPlace() /*-{
	  return this;
	}-*/;

	public final native boolean isPlace() /*-{
	  return this.properties.osm_key === "place";
	}-*/; 

	public final native PhotonFeatureHighwayJSO asHighway() /*-{
	  return this;
	}-*/;
	
	public final native boolean isHighway() /*-{
	  return this.properties.osm_key === "highway";
	}-*/;
}
