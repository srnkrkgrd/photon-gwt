package net.kirkegaard.photongwt.client;

import org.junit.Test;

import com.google.gwt.junit.client.GWTTestCase;

public class PhotonFeatureJSOTest extends GWTTestCase {  
    	
	public final String JSON_RESPONSE = "{\"features\":[{\"properties\":{\"osm_key\":\"place\",\"street\":\"Absalonsvej\",\"state\":\"Capital Region of Denmark\",\"osm_id\":3185238433,\"osm_type\":\"N\",\"housenumber\":\"17\",\"postcode\":\"2990\",\"osm_value\":\"house\",\"city\":\"�v�nget\",\"country\":\"Denmark\"},\"type\":\"Feature\",\"geometry\":{\"type\":\"Point\",\"coordinates\":[12.489554,55.933902]}}]}";

	@Test
	public void testPhotonFeaturesAsValues() {
		PhotonFeaturesJSO features = PhotonFeaturesJSO.fromJson(JSON_RESPONSE);
		assertEquals(features.length(), 1);
		
	}

	@Override
	public String getModuleName() {
		return "net.kirkegaard.photongwt.photongwt";
	}

}
